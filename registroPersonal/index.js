
//Elementos del modal para crear perfil del cliente
let crearNombre = document.getElementById("crearNombre"),
crearFecha = document.getElementById("crearFecha"),
botonCrear = document.getElementById("botonGuardar");

let editarNombre = document.getElementById("editarNombre"),
editarFecha = document.getElementById("editarFecha"),
botoneditar = document.getElementById("botonEditar");




//tabla donde se almacenan los clientes
let padreBaseTabla = document.getElementById("baseTabla").parentNode,
baseTabla = document.getElementById("baseTabla");






//contador de clientes afiliados
let contadorClientes = 0;

//aqui se guadaran los clientes mientras tanto 
let archivoGuardar = [];

let recorrido = 0;





//funcion paa crear el objeto cliente
let guardarCliente = (e) =>{

  function  perfil(nombre,fechaRegistro) {
        this.nombre = nombre;
        this.fechaRegistro = fechaRegistro;
    }

if(crearNombre.value.length == 0 || crearFecha.value == "" ){
    alert("Llene todos los campos para guardar el registro")
    e.preventDefault();
}else if (/^\s+$/.test(crearNombre.value)){
    alert("llene los campos correctamente porfavor");
    e.preventDefault();
}else{

    nuevoCliente = new perfil(crearNombre.value,crearFecha.value);
    
    agregar();
    crearNodoLista();
    limpiarCampos(crearNombre,crearFecha);

}


}

//funcion que agrega las ventas al archvoGuardar
let agregar = () =>{
    archivoGuardar.push(nuevoCliente);
 } 

//funcion para limpiar los campos
let limpiarCampos = (cNombre,cFecha)=>{
    cNombre.value="";
    cFecha.value="";

}

//funcion para crear las listas que se iran mostrando en pantalla  
let crearNodoLista = () =>{
   
    //La columna 
    let columna = document.createElement("tr");
    columna.setAttribute("id",contadorClientes);
   
    //fila 1: Nombre
    filaNombre = document.createElement("td");
    let contenidoFN = document.createTextNode(archivoGuardar[contadorClientes].nombre);
    filaNombre.appendChild(contenidoFN);
   
    //fila 2: fecha
    filaFecha = document.createElement("td");
    let contenidoFF = document.createTextNode(archivoGuardar[contadorClientes].fechaRegistro);
    filaFecha.appendChild(contenidoFF);
   
    //fila 3: enlace de editar y eliminar
    let filaAccion = document.createElement("td");
    let contenidoFA = document.createElement("a");
    contenidoFA.setAttribute("href","#editarRegistro");
    contenidoFA.setAttribute("onclick","enlaceEditar("+contadorClientes+")");
    let textoEdit = document.createTextNode(" Editar  ");
    contenidoFA.appendChild(textoEdit);
    filaAccion.appendChild(contenidoFA);

    let contenidoFa = document.createElement("a");
    contenidoFa.setAttribute("href","#");
    contenidoFa.setAttribute("onclick","borrar("+contadorClientes+")");
    let textoElimi = document.createTextNode("  Eliminar");
    contenidoFa.appendChild(textoElimi);
    filaAccion.appendChild(contenidoFa);
    
    //se agregan los elementos a la columna
    columna.appendChild(filaNombre);
    columna.appendChild(filaFecha);
    columna.appendChild(filaAccion);


    //se agrega al documento
    padreBaseTabla.appendChild(columna);

    contadorClientes++;

}



//----------- A partir de aqui es editar y eliminar clientes -------------------------------



//Funcion para "Eliminar los usuarios de la base de datos y de la tabla "
let borrar = (id) =>{
    let element = document.getElementById(id);
    element.remove();

    recorrido = archivoGuardar.length+recorrido;
for(let i=0;i<recorrido;i++){

      if(id==i){
        archivoGuardar.splice(id,1);
        archivoGuardar.splice(id,0,0);
  }
  
}


}


//funcion del enlace en pantalla para editar el registro
let enlaceEditar = (id)=>{

idRegistroEditar = id;    

editarNombre.value = archivoGuardar[id].nombre

}

//funcion para crear un nuevo perfil en base al perfil anteriormente creado
let editarCliente = () =>{

    function  perfil(nombre,fechaRegistro) {
        this.nombre = nombre;
        this.fechaRegistro = fechaRegistro;
    }

if(editarNombre.value.length == 0 || editarFecha.value == "" ){
    alert("Llene todos los campos para guardar el registro")
    e.preventDefault();
}else if (/^\s+$/.test(crearNombre.value)){
    alert("llene los campos correctamente porfavor");
    e.preventDefault();
}else{

    EDCliente = new perfil(editarNombre.value,editarFecha.value);
    
    cambiarCliente(idRegistroEditar);
    editarNodoLista(idRegistroEditar);

    limpiarCampos(editarNombre,editarFecha);

}

}
//con este se agrega el nuevo perfil a la base de datos 
let cambiarCliente = (id) =>{
    for(let i=0;i<archivoGuardar.length;i++){

        if(id==i){
          archivoGuardar.splice(id,1);
          archivoGuardar.splice(id,0,EDCliente);
          }
    }
}

//ya con esta funcin se modifica la tabla que se muestra en pantalla, con los nuevo valores
let editarNodoLista = (id) =>{
    filaNombre.innerHTML = archivoGuardar[id].nombre;
    filaFecha.innerHTML = archivoGuardar[id].fechaRegistro

}













//-----------Eventos------------------
botonCrear.addEventListener("click",guardarCliente);
botoneditar.addEventListener("click",editarCliente);